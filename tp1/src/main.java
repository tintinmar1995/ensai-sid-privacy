import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class main {

    public static void main(String [ ] args){

        int n = 10000;
        LaunchMe essai1 = new LaunchMe(n,1000, 30, true);
        essai1.print();

        try {

            BufferedWriter writer = new BufferedWriter(new FileWriter("essai.csv"));

            StringBuilder sb = new StringBuilder();
            sb.append("entier, bruit, réponse bruitée\n");

            long[] in = essai1.getSetInteger();
            float[] mid = essai1.getBruit();
            float[] out = essai1.getSetBruite();

            for (int i = 0; i < n; i++) {
                sb.append(in[i]);
                sb.append(',');
                sb.append(mid[i]);
                sb.append(',');
                sb.append(out[i]);
                sb.append('\n');

            }

            writer.write(sb.toString());
            writer.close();

        }catch(IOException e){

            System.out.println("erreur");

        }
    }

}