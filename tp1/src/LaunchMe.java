import java.io.BufferedWriter;
import java.io.FileWriter;

public class LaunchMe {

    public long[] getSetInteger() {
        return setInteger;
    }

    public float[] getBruit() {
        return bruit;
    }

    public float[] getSetBruite() {
        return setBruite;
    }

    long[] setInteger;
    float[] bruit;
    float[] setBruite;
    int n;

    public LaunchMe(int n, int m, float epsilon, boolean test){

        this.n= n;
        setInteger = new long[n];
        bruit = new float[n];
        setBruite = new float[n];
        float temp;

        Laplace mLaplace = new Laplace(epsilon, "unif", test);

        for (int i=0; i<n; i++){
            setInteger[i] = Math.round(Math.random()*m);
            temp = mLaplace.genNoise(20,0.10f,0.01f);
            if (mLaplace.isConsumed()) {
                bruit[i] = 0;
                setBruite[i] = 0;
            }else{
                bruit[i] = temp;
                setBruite[i] = setInteger[i] + temp;
            }
        }

    }


    public void print(){
        System.out.println("Valeurs");
        for(long i:setInteger){
            System.out.println(i);
        }
        System.out.println(" ");
        System.out.println("Bruits");
        for(float i:bruit){
            System.out.println(i);
        }
        System.out.println(" ");
        System.out.println("Bruits");
        for(float i:setBruite){
            System.out.println(i);
        }
    }



}
