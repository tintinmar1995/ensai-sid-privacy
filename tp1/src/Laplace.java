import java.lang.Math;


public class Laplace {

    // L'administrateur fixe epsilon max : le bruit maximal toléré par l'admin
    // Au délas d'epsilon, on transmet trop d'info
    // Comme on divise par epislon dans laplace, la bruitage est inversemment proport à epsilon
    float epsilon_max;
    boolean consumed = false;
    float epsilon_cum;
    float epsilon_suite;
    String type;
    boolean test;

    public Laplace(float epsilon, String type, boolean test){
        this.epsilon_max = epsilon;
        this.epsilon_suite = epsilon;
        this.type = type;
        this.test = test;
    }

    // On ajoute le bruit au résultat d'une requête statistique
    // Requête = Aggrégat sur l'échantillon
    // Sensitivité : impact maximal d'un individu dans un échantillon
    // Sensititvité : bornes de la densité de Laplace
    public float genNoise(int sensitivity, float fracOfEps, float eps){

        float bruit;
        float epsilon = 0;

        if (type.equals("unif")) {
            // On prend une part de l'epsilon total
            epsilon = epsilon_max * fracOfEps;
        } else if (type.equals("user")) {
            // On laisse à l'utilisateur le choix de l'epsilon
            epsilon = eps;
        } else if (type.equals("conv")){
            // On choisit une suite d'epsilon convergeant vers 1
            epsilon = epsilon_suite;
            epsilon_suite = epsilon_suite / 2;
        }

        if(!consumed) {
            // On vérifie qu'on ne livre pas trop d'information à l'utilisateur
            // Si l'utilisateur effectue 20 requête alors qu'on lui en avait permis 10
            // Il peut récupérer des infos privées que nous voulions cacher
            // Ex: En faisant 20 fois la même requête et en moyennant
            epsilon_cum = epsilon_cum + epsilon;
            if(!test) {
                consumed = epsilon_cum >= epsilon_max;
            }

            bruit = genLaplace(0, sensitivity / epsilon);

        }else{
            // Normalement, il faudrait jeter une exception mais flemme de coder cela
            bruit = -100000;
        }

        return bruit;
    }

    // Le bruit est généré peu importe le epsilon par une loi de Laplace
    private float genLaplace(float mu, float b){
        double u = Math.random()-0.5;
        double retour = mu - b * u/Math.abs(u) * Math.log(1-2*Math.abs(u));
        float retourr = (float) retour;
        return retourr;
    }

    public boolean isConsumed() {
        return consumed;
    }

}
