package function;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

import peersim.cdsim.CDProtocol;
import peersim.config.FastConfig;
import peersim.core.CommonState;
import peersim.core.Linkable;
import peersim.core.Network;
import peersim.core.Node;
import utils.Pair;
import utils.encryption.damgardjurik.Cryptosystem.PrivateKey;
import utils.encryption.damgardjurik.Cryptosystem.PublicKey;


public class Template_Function 
implements CDProtocol, ParticipantIF 
{

	private static final Logger logger = Logger.getLogger(Template_Function.class);
	private final static String resultsDir = "./results/";
	private BufferedWriter wResults;
	
	
    public Template_Function(String prefix) 
    {
    	
    	/* ******************
		 * BEGIN CUSTOMIZE
		 */
		
		String functionName = "templ";  
    	
		/* 
		 * END CUSTOMIZE
		 ****************** */
    	
		    	
        Date now = new Date () ; 
		SimpleDateFormat dateFormatter = new SimpleDateFormat ( "yyyy_MM_dd_HH_mm" );
    	try 
        {
			this.wResults = 
					new BufferedWriter ( 
								new FileWriter ( 
										new File ( Template_Function.resultsDir + functionName + "-" + 
												Network.size() + 
												"-" + 
												dateFormatter.format(now.getTime()) + 
												".csv" ), 
										true ) );
		} 
        catch ( IOException e ) 
        {
			e.printStackTrace();
		}
    	
    }
	
    
    
    
	@Override
	public void nextCycle(Node node, int protocolID)
	{
		int linkableID = FastConfig.getLinkable(protocolID);
	    Linkable linkable = (Linkable) node.getProtocol(linkableID);
	        
		if (linkable.degree() > 0) 
        {
			Node peer = linkable.getNeighbor(CommonState.r.nextInt(linkable
                    .degree()));

            if (!peer.isUp())
                return;

            Template_Function neighbor = (Template_Function) peer
                    .getProtocol(protocolID);

            
            
        	/* ******************
    		 * BEGIN CUSTOMIZE
    		 */
    		
            // The participant's actions are coded here.  
            
    		/* 
    		 * END CUSTOMIZE
    		 ****************** */
            
        }
	}
	
	
	
	
	/* ******************
	 * BEGIN CUSTOMIZE
	 */
	
	
	@Override
	public Object clone()
	{
		Template_Function c = null; 
		
		try {
			c = (Template_Function) super.clone();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
				
		return c; 
	}


	@Override
	public void setData(Integer data) 
	{
		// TODO Auto-generated method stub
		
	}


	@Override
	public Integer getData() 
	{
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public void setKeys(Pair<PublicKey, PrivateKey> keys) 
	{
		// TODO Auto-generated method stub

	}


	@Override
	public Pair<PublicKey, PrivateKey> getKeys() 
	{
		// TODO Auto-generated method stub
		return null;
	}

	
	/* 
	 * END CUSTOMIZE
	 ****************** */
	
}
