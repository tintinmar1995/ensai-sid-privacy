package function;

import java.util.List;

import utils.Pair;
import utils.encryption.damgardjurik.Cryptosystem.PrivateKey;
import utils.encryption.damgardjurik.Cryptosystem.PublicKey;


/**
 * This interface defines the methods that a Peersim peer that holds  
 * local data must implement. The method names are given to the 
 * Peersim simulator through the configuration file. 
 * 
 * @author tallard
 */
public interface ParticipantIF 
{
	/**
	 * Method called by the Peersim simulator to assign a data 
	 * object to a peer during the initialization of the network. 
	 * 
	 * @param data The data assigned. 
	 */
	public void setData ( Integer data );
	
	/**
	 * Method used from the Peersim simulator to get a peer's data. 
	 * 
	 * @return The local data. 
	 */
	public Integer getData ( ); 
	
	/**
	 * Set the cryptographic keys. This method is called by the 
	 * framework during the initialization of a peer. 
	 * 
	 * @param keys The cryptographic keys. 
	 */
	public void setKeys ( Pair<PublicKey, PrivateKey> keys );
	
	/**
	 * Get the cryptographic keys of this peer. 
	 * 
	 * @return The cryptographic keys. 
	 */
	public Pair<PublicKey, PrivateKey> getKeys ( );
	
}
