package launcher;

import java.io.PrintStream;

import org.apache.log4j.Logger;

import peersim.cdsim.CDSimulator;
import peersim.config.Configuration;
import peersim.config.IllegalParameterException;
import peersim.config.MissingParameterException;
import peersim.config.ParsedProperties;
import peersim.core.CommonState;
import peersim.core.Network;
import peersim.edsim.EDSimulator;

// Code copied from the Simulator class from peersim and adapted to our context.
/**
 * The launcher for a Peersim simulation of the distributed kmeans
 * algorithm. 
 * 
 * @author tallard
 * 
 */
public class Launchme {

	private static final Logger logger = Logger.getLogger(Launchme.class);

	// ========================== static constants ==========================
	// ======================================================================

	/** {@link CDSimulator} */
	public static final int CDSIM = 0;

	/** {@link EDSimulator} */
	public static final int EDSIM = 1;

	/** Unknown simulator */
	public static final int UNKNOWN = -1;

	/** the class names of simulators used */
	protected static final String[] simName = { "peersim.cdsim.CDSimulator",
			"peersim.edsim.EDSimulator", };

	/**
	 * Parameter representing the number of times the experiment is run.
	 * Defaults to 1.
	 * 
	 * @config
	 */
	public static final String PAR_EXPS = "simulation.experiments";

	/**
	 * If present, this parameter activates the redirection of the standard
	 * output to a given PrintStream. This comes useful for processing the
	 * output of the simulation from within the simulator.
	 * 
	 * @config
	 */
	public static final String PAR_REDIRECT = "simulation.stdout";

	// ==================== static fields ===================================
	// ======================================================================

	/** */
	private static int simID = UNKNOWN;

	// ========================== methods ===================================
	// ======================================================================

	/**
	 * Returns the numeric id of the simulator to invoke. At the moment this can
	 * be {@link #CDSIM}, {@link #EDSIM} or {@link #UNKNOWN}.
	 */
	public static int getSimID() {

		if (simID == UNKNOWN) {
			if (CDSimulator.isConfigurationCycleDriven()) {
				simID = CDSIM;
			} else if (EDSimulator.isConfigurationEventDriven()) {
				simID = EDSIM;
			}
		}
		return simID;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		logger.info("loading configuration");
		Configuration.setConfig(new ParsedProperties(args));

		PrintStream newout = (PrintStream) Configuration.getInstance(
				PAR_REDIRECT, System.out);
		if (newout != System.out)
			System.setOut(newout);

		final int SIMID = getSimID();
		
		if (SIMID == UNKNOWN) 
		{
			logger.fatal("Unable to determine simulation engine type");
			return;
		}

		try {
			long seed = CommonState.r.nextLong();
			CommonState.initializeRandom(seed);		
			logger.info("Starting experiment invoking "
						+ simName[SIMID]);
			CDSimulator.nextExperiment();
		} catch (MissingParameterException e) {
			logger.fatal(e + "");
			// System.exit(1);
		} catch (IllegalParameterException e) {
			logger.fatal(e + "");
			// System.exit ( 1 );
		}

	}

}
